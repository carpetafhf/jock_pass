// C++ program to create target string, starting from 
// random string using Genetic Algorithm 

#include <bits/stdc++.h> 
#include <fstream>
#include <string>
#include <iostream>
#include <regex>

using namespace std;

// Number of individuals in each generation
#define POPULATION_SIZE 100

// Valid Genes
const string GENES = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890, .-;:_!\"#%&/()=?@${[]}";

// Target string to be generated
string TARGET = "";

// Function to generate random numbers in given range
int random_num(int start, int end)
{
	int range = (end - start) + 1;
	int random_int = start + (rand() % range);
	return random_int;
}

void find_Numbers(string pass)
{
	std::string pattern("[0-9]+");         // Regex expression
    	std::regex rx(pattern);             // Getting the regex object

    	std::ptrdiff_t number_of_matches = std::distance(  // Count the number of matches inside the iterator
        std::sregex_iterator(pass.begin(), pass.end(), rx),
        std::sregex_iterator());

    	std::cout << number_of_matches << std::endl;  // Displaying results
}

// Create random genes for mutation
char mutated_genes()
{
	int len = GENES.size();
	int r = random_num(0, len - 1);
	return GENES[r];
}

// create chromosome or string of genes
string create_gnome()
{
	int len = TARGET.size();
	string gnome = "";
	for (int i = 0; i<len; i++)
		gnome += mutated_genes();
	return gnome;
}

//Return true if char c represents a digit 
bool isNumber(char c)
{
	if(c < '0' || c > '9') return false;
	return true;
}

// Class representing individual in population
class Individual
{
public:
	string chromosome;
	int fitness;
	Individual(string chromosome);
	Individual mate(Individual parent2);
	int cal_fitness();
};

Individual::Individual(string chromosome)
{
	this->chromosome = chromosome;
	fitness = cal_fitness();
};

// Perform mating and produce new offspring
Individual Individual::mate(Individual par2)
{
	// chromosome for offspring
	string child_chromosome = "";

	int len = chromosome.size();
	for (int i = 0; i<len; i++)
	{
		// random probability
		float p = random_num(0, 100) / 100;

		// if prob is less than 0.45, insert gene
		// from parent 1
		if (p < 0.45)
			child_chromosome += chromosome[i];

		// if prob is between 0.45 and 0.90, insert
		// gene from parent 2
		else if (p < 0.90)
			child_chromosome += par2.chromosome[i];

		// otherwise insert random gene(mutate),
		// for maintaining diversity
		else
			child_chromosome += mutated_genes();
	}

	// create new Individual(offspring) using
	// generated chromosome for offspring
	return Individual(child_chromosome);
};


// Calculate fittness score, it is the number of
// characters in string which differ from target
// string.
int Individual::cal_fitness()
{
	int len = TARGET.size();
	int fitness = 0;

	int sizeleet = 5;
	char leet1[sizeleet]={'a','b','e','o','s','a','l','t','i'};
	char leet2[sizeleet]={'4','8','3','0','5','@','1','7','1'};

	for (int i = 0; i<len; i++)
	{
		bool match = false;
		if (chromosome[i] != TARGET[i])
		{
			//FHF: We do a check against the leet substitutions defined in leet arrays
			for(int j=0; j<sizeleet; j++)
			{
				//if condition to check if there is a leet substitution
				if (tolower(chromosome[i]) == leet1[j] && tolower(TARGET[i]) == leet2[j] || tolower(chromosome[i]) == leet2[j] && tolower(TARGET[i]) == leet1[j])
				{
					match = true;
					break;
				}
			}
			
			if(!match) 
			{
				
				//we also allow the change of one number by another
				if(isNumber(chromosome[i]) && isNumber(TARGET[i])){}
				else fitness++;
			}
		}
	}
	return fitness;
};

// Overloading < operator
bool operator<(const Individual &ind1, const Individual &ind2)
{
	return ind1.fitness < ind2.fitness;
}

// Driver code
int main(int argc, char* argv[])
{
	if (argc < 3) {
		// Tell the user how to run the program
		cout << "Usage: " << argv[0] << " {pass} {fitness}" << endl;

		return 1;
	}
	
	string pass = argv[1];
	int c_fit = atoi(argv[2]);
	//FHF: new argument -> Minimun number of results allowed
	int minresults = atoi(argv[3]);

	TARGET = pass;

	ofstream myfile;
	string candidates = "";

	//FHF: I prefer to add the candidates to a vector, so I can 
	//search in runtime to avoid duplicates
	vector<string> candidates2;

	srand((unsigned)(time(0)));

	// current generation
	int generation = 0;

	vector<Individual> population;
	bool found = false;

	// create initial population
	for (int i = 0; i<POPULATION_SIZE; i++)
	{
		string gnome = create_gnome();
		population.push_back(Individual(gnome));
	}

	//FHF : New finish conditions. We stop when we reach the minimun results required
	//or when we reach 10K generations. 
	while (candidates2.size() < minresults || generation < 10000)
	{
		// sort the population in increasing order of fitness score
		sort(population.begin(), population.end());

		// Otherwise generate new offsprings for new generation
		vector<Individual> new_generation;

		// Perform Elitism, that mean 10% of fittest population
		// goes to the next generation
		int s = (10 * POPULATION_SIZE) / 100;
		for (int i = 0; i<s; i++)
			new_generation.push_back(population[i]);

		// From 50% of fittest population, Individuals
		// will mate to produce offspring
		s = (90 * POPULATION_SIZE) / 100;
		for (int i = 0; i<s; i++)
		{
			int len = population.size();
			//FHF: deleted constant 50, replaced by POPULATION_SIZE/2 
			int r = random_num(0, (int)POPULATION_SIZE/2);
			Individual parent1 = population[r];
			r = random_num(0, (int)POPULATION_SIZE / 2);
			Individual parent2 = population[r];
			Individual offspring = parent1.mate(parent2);
			new_generation.push_back(offspring);
		}
		population = new_generation;
		cout << "Generation: " << generation << "\t";
		cout << "String: " << population[0].chromosome << "\t";
		cout << "Fitness: " << population[0].fitness << "\n";

		//FHF: Search in ALL population individuals, not only the first
		for (int i = 0; i < population.size(); i++)
		{
			//FHF: We collect all individuals with a fitness less or equal than the desired fitness
			//if (population[i].fitness > 0 && population[i].fitness <= c_fit)
			if(population[i].fitness <= c_fit)
			{
				//FHF: If the individual meet the fitness requirements and does not exist in the candidates vector
				//we add it to the list
				if (std::find(candidates2.begin(), candidates2.end(), population[i].chromosome) == candidates2.end())
				{
					candidates2.push_back(population[i].chromosome);
				}
			}
		}


		generation++;
	}

	cout << "Generation: " << generation << "\t";
	cout << "String: " << population[0].chromosome << "\t";
	cout << "Fitness: " << population[0].fitness << "\n";


	myfile.open("candidates.txt");

	//FHF: Add all the candidates2 elements to the result file
	for (int i = 0; i < candidates2.size(); i++)
	{
		myfile << candidates2.at(i) << endl;
	}

	myfile.close();
}

